(function() {
  var app, express, host, port, stylus, jade, program, fs, color;

  fs = require('fs');
  stylus = require('stylus');
  jade = require('jade');
  program = require('commander');
  color = require('color');
  express = require('express');
  app = express.createServer();

  program
    .version('1.0')
    .usage('[options]')
    .option('-p, --port <port>', 'Puerto donde correra el servidor [8000]',
      Number, 8000)
    .option('-h, --host <IP>', 'IP donde correra el servidor [0.0.0.0]',
      String, '0.0.0.0')
    .parse(process.argv);


  function compileMethod(str) {
    return stylus(str, {
      compress: true
    });
  }

  function init(config) {
    config = config || {};

    app.use(stylus.middleware({
      debug: true,
      src: config.dirname,
      dest: config.dirname,
      compile: compileMethod
    }));

    app.configure(function() {
      app.set('view', config.dirname);
      app.set('view options', {
        layout: false,
        prety: true,
        local: {
          debug: true,
          time: function() {
            return new Date().getTime()
          },
          options: config.options || 0
        }
      });

      if (config.jade !== false) {
        app.set('views engine', 'jade');
      }

      return app.use(express.static(config.dirname||config.static));
    });

    app.listen(program.port, program.host);

    console.log(
      [color.set('Sirviendo -> ', 'gray'), color.set('%s', 'cyan')].join(''),
      [program.host, program.port].join(':')
    );
  }

  exports.app = app;
  exports.init = init;

}).call(this);
