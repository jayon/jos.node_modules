(function() {
  var fs = require('fs');
  var util = require('util');
  var jade = require('jade');
  var mkdirp = require('mkdirp');
  var uglifyjs = require('uglify-js');
  var findit = require('findit');
  var stylus = require('stylus');
  var path = require('path');
  var basename = path.basename;
  var dirname = path.dirname;
  var resolve = path.resolve;
  var wrench = require('wrench');
  
  var color = require('color');

  /* Recorre el sistema de archivos buscando carpetas y archivos */
  function FI() {
    var self = this;

    self.re = /.*/;

    self.path = __dirname + '/static';

    self.onFile = function() {
    };

    self.init = function(path) {
      fs.lstat(path, function(err, stat) {
        if (err) {
          throw err;
        }

        if (stat.isFile() && self.re.test(path)) {
          fs.readFile(path, 'utf8', function(err, str) {
            if (err) {
              throw err;
            }

            if (typeof self.onFile === 'function') {
              self.onFile(path, str);
            }
          });
        }

        else if (stat.isDirectory()) {
          fs.readdir(path, function(err, files) {
            if (err) {
              throw err;
            }

            files.map(function(filename) {
              return path + '/' + filename;
            })
            .forEach(self.init);
          });
        }
      });
    }
  }

  /* copia y pega archivos */
  function copy(src, dst, cb) {
    if (fs.lstatSync(src).isDirectory()) {
      wrench.copyDirSyncRecursive(src, dst);
    }

    else {
      fs.stat(dst, _copy);
    }

    return;

    function _copy(err) {
      var is, os;

      if (!err) {
        if (cb) {
          return cb(new Error("File " + dst + " exists."));
        }
        else {
          console.log(new Error("File " + dst + " exists."));
        }
      }

      fs.stat(src, function (err) {
        if (err) {
          return cb(err);
        }
        is = fs.createReadStream(src);
        os = fs.createWriteStream(dst);
        util.pump(is, os, cb);
      });
    }
  }

  /* exporta archivos jade a html */
  function templates(opt) {
    opt = opt || {};
    var origin = opt.origin || __dirname + '/templates';
    var destino = opt.destino || __dirname + '/build';
    var build_ext_file = opt.build_ext_file || '.html';
      
    var cjade = opt.jade || {};
    cjade.layout = cjade.layoud || false;
    cjade.prety = cjade.prety || true;
    cjade.time = cjade.time || function() {
      return new Date().getTime()
    };
    

    var w = new FI();
    w.onFile = function(file, str) {
      if (typeof opt.validFile === 'function' && opt.validFile(file) === true) {
        var options = cjade;
        options.filename = file;

        var fn = jade.compile(str, options);
        var nfile = file.split(origin).join(destino);

        if (file.indexOf('inmueble') !== -1) {
          nfile = nfile.split('.jade').join('.jinja');
        }

        else if (build_ext_file) {
          nfile = nfile.split('.jade').join(build_ext_file);
        }

        else {
          nfile = nfile.split('.jade').join('.html');
        }

        var dir = resolve(dirname(nfile));

        mkdirp(dir, 0755, function(err) {
          if (err) {
            throw err;
          }

          fs.writeFile(nfile, fn(options), function(err) {
            if (err) {
              throw err;
            }
            console.log(
              color.set('Template', 'blue'),
              color.key('export', file),
              color.key('to', nfile));
          });
        });
      }

      else {
        console.log(
          color.set('Template', 'blue'),
          color.key('skip', file, 'red')
        );
      }
    };

    w.init(origin);
  }

  /* minimizar javascript */
  function minJS(file, debug) {
    var jsp = uglifyjs.parser;
    var pro = uglifyjs.uglify;

    var orig_code = fs.readFileSync(file, 'utf8');

    if (debug) {
      return orig_code;
    }

    var ast = jsp.parse(orig_code); // parse code and get the initial AST
    ast = pro.ast_mangle(ast); // get a new AST with mangled names
    ast = pro.ast_squeeze(ast); // get an AST with compression optimizations
    return pro.gen_code(ast); // compressed code here
  }
  
  function javascript(opt) {
    opt = opt || {};
    var origin = opt.origin;
    var destino = opt.destino;

    findit.find(origin, function(file) {

      if (file.indexOf('.js') !== -1 && file.indexOf(opt.exclude) === -1) {

        var final_code = minJS(file);
        var destino_file = file.split(origin).join(destino);

        fs.writeFile(destino_file, final_code, function(err) {
          if (err) throw err;
          console.log(
            color.set('JS', 'blue'), 
            color.key('min', file),
            color.key('to', destino_file)
            );
        });
      }

      else {
        console.log(
          color.set('JS', 'blue'), 
          color.key('skip', file, 'red')
        );
      }
    });
  }

  function merge(opt) {
    opt = opt || {};
    var origin = opt.origin;
    var destino = opt.destino;
    var final_code = '';
    var i = 0;

    for (i; i < origin.length; ++i) {
      final_code += fs.readFileSync(origin[i], 'utf8');
    }

    fs.writeFileSync(destino, final_code, 'utf8');

    console.log(
      color.key('merge', '[' + origin.join(',\n') + ']'),
      color.key('to', destino)
    );

    if (typeof opt.callback === 'function') {
      opt.callback();
    }
  }
  
  function styles(opt) {
    opt = opt || {};
    var origin = opt.origin;
    var destino = opt.destino;

    var site = fs.readFileSync(origin, 'utf8');
    stylus(site)
      .set('filename', origin)
      .set('compress', opt.compress || true)
      .render(function(err, css){
        if (err) throw err;

        fs.writeFile(destino, css, function(err) {
          if (err) throw err;
          console.log(
            color.set('Estilos', 'blue'), 
            color.key('copy', origin),
            color.key('->', destino)
            );
        });
      });
  }

  function prepare(opt) {
    var i;

    try {
      // eliminar
      if (opt.remove && opt.remove.length > 0) {
        for (i in opt.remove) {
          if (
            opt.remove.hasOwnProperty(i) &&
            fs.lstatSync(opt.remove[i]).isDirectory()
          ) {
            wrench.rmdirSyncRecursive(opt.remove[i]);
          }
        }
      }
    }
    catch(e) {
      console.log(e);
    }

    try {
      // crear
      if (opt.create && opt.create.length > 0) {
        for (i in opt.create) {
          wrench.mkdirSyncRecursive(opt.create[i]);
        }
      }
    }
    catch(e) {
      console.log(e);
    }
  }
  

  exports.templates = templates;
  exports.javascript = javascript;
  exports.styles = styles;
  exports.prepare = prepare;
  exports.copy = copy;

}).call(this);
